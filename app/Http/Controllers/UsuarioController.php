<?php

namespace App\Http\Controllers;

use App\User;
use App\Usuario;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function getAll(){

        $usuarios = User::all();


        return view('listaUsuario', [
            'usuarios' => $usuarios
        ]);

    }

    public function getUsuario( Usuario $usuario){

        //$usuario = Usuario::where('id_usuario', '=', 1)->first();
        //dd($usuario);
        return view('visualizaUsuario', [
            'usuario' => $usuario
        ]);

    }

    public function formAddUsuario(){
        return view('insereUsuario');
    }

    public function create(Request $request)
    {
        //CRIANDO USUARIO
        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        return redirect()->route('dashboard');

    }

    public function createUsuario(Request $request){
        //dd($request); vardump

        //METODO TESTE CRIAR USUARIO

        $usuario = new Usuario();
        $usuario->usuario = $request->usuario;
        $usuario->login = $request->login;
        $usuario->senha = Hash::make( $request->senha );
        $usuario->tipo = $request->tipo;
        $usuario->save();

        return redirect()->route('users.getAll');
        
    }

    public function formEditUsuario(Usuario $usuario){
        //dd($usuario);
        return view('editUsuario', [
            'usuario' => $usuario
        ]);

    }

    public function edit(){

    }

    public function createCliente(){
        
        //CRIAR CLIENTE
        $usuario = new Usuario();
        $usuario->nome = "Fernanda Scopel";
        $usuario->cpf = "01245784562";
        $usuario->estado = "RS";
        $usuario->cidade = "Santo Angelo";

        $usuario->save();
    }


}
