<?php

namespace App\Http\Controllers\Api;

//use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;


//MODULO PARA AUTENTICACAO

class AuthController extends Controller
{
    public function dashboard()
    {
        if(Auth::check() === true){
            
            return view('admin.dashboard');

        }else{
            return view('layouts\\app');
        }
        
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']); //nunca salve uma senha sem fazer o hash, sempre vai dar FALSO
        
        Auth::attempt($credentials);
        /* ABAIXO PARA APIS
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        
        return $this->respondWithToken($token);
        */
        
    }

    
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
} 
