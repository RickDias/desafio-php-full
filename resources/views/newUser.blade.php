<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('users.saveUser') }}" method="POST">
                @csrf

                <label for="usuario">
                    Nome Usuário
                </label>
                <input type="text" class="form-control" id="usuario" name="usuario" />
                <label for="login">
                    Login Usuário
                </label>
                <input type="text" class="form-control" id="login" name="login"/>

                <label for="senha">
                    Password
                </label>
                <input type="password" class="form-control" id="senha" name="senha"/>

                <label for="tipo">
                    Tipo Usuario
                </label>
                <input type="text" class="form-control" id="tipo" name="tipo"/>

                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
            </form>
        </div>
    </div>
</div>