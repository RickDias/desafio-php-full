<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="titulo">
                <h3>
                    h3. Lorem ipsum dolor sit amet.
                </h3>
                <div class="row">
                    <div class="col-md-4">
                        <button type="button" class="btn btn-success">
                            Incluir
                        </button>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-success">
                            Alterar
                        </button>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-success">
                            Excluir
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-success">
                            Consultar
                        </button>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-success">
                            Listagem
                        </button>
                    </div>
                </div>
                <button type="button" class="btn btn-success">
                    Button
                </button>
            </div>
        </div>
    </div>
</body>

</html>