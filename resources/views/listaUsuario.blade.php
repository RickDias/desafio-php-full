@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Listagem') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif


                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Estado</th>
                            </tr>
                        </thead>


                        <tbody>
                            @foreach ($usuarios as $usuario )
                            <tr>
                                <td>{{ $usuario->id }}</td>
                                <td>{{ $usuario->name }}</td>
                                <td>{{ $usuario->email }}</td>
                                <td>{{ $usuario->estado }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <a href="{{ route('dashboard') }}"  class="btn btn-success">
                                Voltar
                            </a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection