<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="titulo">
                <h3>
                    Bem Vindo
                </h3>
                <div>
                        <button type="button" class="btn btn-success">
                            Entrar
                        </button>
                </div>
                <button type="button" class="btn btn-success">
                    Button
                </button>
            </div>
        </div>
    </div>
</body>

</html>