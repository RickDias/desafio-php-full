<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('users.saveUser') }}" method="POST">
                @csrf
                
                <label for="usuario">
                    Nome Usuário {{ $usuario->usuario }}
                </label>
                <input type="text" class="form-control" id="usuario" name="usuario" value="{{ $usuario->nome }}"/>
                <label for="login">
                    Login Usuário
                </label>
                <input type="text" class="form-control" id="login" name="login" value="{{$usuario->login}}"/>

                <label for="senha">
                    Password
                </label>
                <input type="password" class="form-control" id="senha" name="senha" value="{{$usuario->senha}}"/>

                <label for="tipo">
                    Tipo Usuario
                </label>
                <input type="text" class="form-control" id="tipo" name="tipo" value="{{$usuario->tipo}}"/>

                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
            </form>
        </div>
    </div>
</div>