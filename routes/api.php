<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::post('auth/entrar', 'Api\\AuthController@formLogin');


Route::post('auth/login', 'Api\\AuthController@login')->name('login');

Route::group(['middleware'=> ['apiJwt']], function(){
  
    Route::get('usuarios', 'Api\\UsuarioController@getUser')->name('users.getAll');
    Route::get('usuarios/novo', 'Api\\UsuarioController@formAddUsuario')->name('users.formAdd');
    Route::get('usuarios/editar/{user}', 'Api\\UsuarioController@formEditUsuario')->name('users.editar');
    Route::get('usuarios/{user}', 'Api\\UsuarioController@getUsuario')->name('users.getUser');

    Route::post('usuarios/saveUser', 'UsuarioController@createUsuario')->name('users.saveUser');
    
    Route::put('usuarios/edit/{user}', 'UsuarioController@edit')->name('users.edit');
});
