<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts\\app');
});

/**
 * VERBO GET - ORGANIZAR ROTAS POR VERBOS
 */
Route::get('admin', 'Api\\AuthController@dashboard')->name('dashboard');
Route::get('usuarios/novo', 'UsuarioController@formAddUsuario')->name('users.formAdd');
Route::get('usuarios/novo/salva', 'UsuarioController@create')->name('users.AddSave');

Route::get('usuarios', 'UsuarioController@getAll')->name('users.getAll');

Route::get('usuarios/editar/{user}', 'UsuarioController@formEditUsuario')->name('users.editar');
Route::get('usuarios/{user}', 'UsuarioController@getUsuario')->name('users.getUser');

/**
 * VERBO POST
 */
Route::post('usuarios/saveUser', 'UsuarioController@createUsuario')->name('users.saveUser');
//Route::get('cria-usuario', 'UsuarioController@createUsuario');

/**
  * VERBO PUT/PATCH
  */
Route::put('usuarios/edit/{user}', 'UsuarioController@edit')->name('users.edit');

  /**
   * VERBO DELETE
   */

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
